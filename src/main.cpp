#include "main.h"


/////
// For instalattion, upgrading, documentations and tutorials, check out website!
// https://ez-robotics.github.io/EZ-Template/
/////

// 3rd place in the chassis constructor list
#define LEFT_LIFT 2
#define RIGHT_LIFT 2

// Chassis constructor
Drive chassis(
    // Left Chassis Ports (negative port will reverse it!)
    //   the first port is the sensored port (when trackers are not used!)
    {-11, -6, -10}

    // Right Chassis Ports (negative port will reverse it!)
    //   the first port is the sensored port (when trackers are not used!)
    ,
    {13, 16, 18}

    // IMU Port
    ,
    1

    // Wheel Diameter (Remember, 4" wheels are actually 4.125!)
    //    (or tracking wheel diameter)
    ,
    2.75

    // Cartridge RPM
    //   (or tick per rotation if using tracking wheels)
    ,
    600

    // External Gear Ratio (MUST BE DECIMAL)
    //    (or gear ratio of tracking wheel)
    // eg. if drive is 84:36 where the 36t is powered, RATIO would be 2.333.
    // eg. if drive is 36:60 where the 60t is powered, RATIO would be 0.6.
    ,
    1.333333333333);

// flywheel
sylib::SpeedControllerInfo
    flywheelPID([](double rpm) { return std::pow(M_E, (-0.001 * rpm + 1)) + 6.13; }, // kV function
                5, // kP
                0.001, // kI
                0, // kD
                0, // kH
                true, // anti-windup enabled
                50, // anti-windup range
                true, // p controller bounds threshold enabled
                50, // p controller bounds cutoff enabled
                0.01, // kP2 for when over threshold
                100, // range to target to apply max voltage
                false, // coast down enabled
                0, // coast down threshold
                1); // coast down constant

// rotation sensor
pros::Rotation liftRotation(15);

// other motors
pros::Motor intake(2, pros::E_MOTOR_GEARSET_06);
sylib::Motor flywheel = sylib::Motor(4, 1800, true, flywheelPID);

// pneumatics
pros::ADIDigitalOut wingsPiston('d');
pros::ADIDigitalOut lockerPiston('c');
pros::ADIDigitalOut ptoPiston('b', false);

// globals
bool wingsPistonState = false;
bool lockerPistonState = false;
bool ptoOnLift = false;

// pros::Task* lift_auto_task = nullptr

double lift_position() {
    double angle = liftRotation.get_angle() / 100;
    return (angle > 300 ? 0 : angle);
}

// Function to toggle the Power Take-Off (PTO)
void togglePTO(bool active) {
  // If the current state is the same as the desired state, no need to change
  // anything
  if (active == ptoOnLift)
    return;

  // Update the PTO piston state
  ptoOnLift = active;

  // Toggle the PTO on the chassis, affecting the left and right lift motors
  if (active)
    chassis.pto_add(
        {chassis.left_motors[LEFT_LIFT], chassis.right_motors[RIGHT_LIFT]});
  else
    chassis.pto_remove(
        {chassis.left_motors[LEFT_LIFT], chassis.right_motors[RIGHT_LIFT]});

  // Set the value of the PTO piston
  ptoPiston.set_value(active);

  // If the PTO is active or if the robot is in autonomous mode
  if (active || pros::competition::is_autonomous()) {
    // Set the brake mode of the left and right lift motors to hold
    chassis.left_motors[LEFT_LIFT].set_brake_mode(pros::E_MOTOR_BRAKE_HOLD);
    chassis.right_motors[RIGHT_LIFT].set_brake_mode(pros::E_MOTOR_BRAKE_HOLD);
  } else {
    // Otherwise, set the brake mode of the left and right lift motors to coast
    chassis.left_motors[LEFT_LIFT].set_brake_mode(pros::E_MOTOR_BRAKE_COAST);
    chassis.right_motors[RIGHT_LIFT].set_brake_mode(pros::E_MOTOR_BRAKE_COAST);
  }
}

// Function to set the speed of the lift
void setLiftSpeed(int input) {
  // If the input speed is 0
  if (input == 0) {
    // If the PTO piston is not active
    if (ptoOnLift) {
      // Stop the left and right lift motors
      chassis.left_motors[LEFT_LIFT] = 0;
      chassis.right_motors[RIGHT_LIFT] = 0;
    }
    // Exit the function
    return;
  }

  // Deactivate the PTO
  togglePTO(true);
  // Set the speed of the left and right lift motors to the negative of the
  // input Negative is used to make the lift go up when a positive speed is
  // input
  chassis.left_motors[LEFT_LIFT].move_velocity(-input);
  chassis.right_motors[RIGHT_LIFT].move_voltage(-input);
}

double liftBangBang(double position, double target) {
    double error = target - position;
    double speed = 0;
    if (error > 0) {
        speed = 12000;
    } else if (error < 0) {
        speed = -12000;
    } else {
        speed = 0;
    }
    return speed;
}

void liftTo(double targetValue) {
    while (fabs(lift_position() - targetValue) > 10) {
        double output = liftBangBang(lift_position(), targetValue);
        setLiftSpeed(output);
        std::cout << lift_position() << std::endl;
        pros::delay(ez::util::DELAY_TIME);
    }
    setLiftSpeed(0);
}

/**
 * Runs initialization code. This occurs as soon as the program is started.
 *
 * All other competition modes are blocked by initialize; it is recommended
 * to keep execution time for this mode under a few seconds.
 */
void initialize() {
    // Print our branding over your terminal :D
    sylib::initialize();
    ez::print_ez_template();

    pros::delay(500); // Stop the user from doing anything while legacy ports configure.

    // Configure your chassis controls
    chassis.toggle_modify_curve_with_controller(
        false); // Enables modifying the controller curve with buttons on the joysticks
    chassis.set_active_brake(0); // Sets the active brake kP. We recommend 0.1.
    chassis.set_curve_default(0, 0); // Defaults for curve. If using tank, only the first parameter is used. (Comment
                                     // this line out if you have an SD card!)
    default_constants(); // Set the drive to your own constants from autons.cpp!
    exit_condition_defaults(); // Set the exit conditions to your own constants from autons.cpp!

    // These are already defaulted to these buttons, but you can change the left/right curve buttons here!
    // chassis.set_left_curve_buttons (pros::E_CONTROLLER_DIGITAL_LEFT, pros::E_CONTROLLER_DIGITAL_RIGHT); // If using
    // tank, only the left side is used. chassis.set_right_curve_buttons(pros::E_CONTROLLER_DIGITAL_Y,
    // pros::E_CONTROLLER_DIGITAL_A);

    // Autonomous Selector using LLEMU
    ez::as::auton_selector.add_autons({
        Auton("Opponent Side", opponentSide),
       Auton("Skills", skillsAuton),
        Auton("Alliance Side", allianceSide),
        Auton("None", noAuton),
    });

    // Initialize chassis and auton selector
    liftRotation.reset_position();

    // Initialize flywheel
    flywheel.tare_encoder();

    // Initialize chassis and auton selector
    // togglePTO(ptoPistonState);
    chassis.pto_remove(
      {chassis.left_motors[LEFT_LIFT], chassis.right_motors[RIGHT_LIFT]});

    chassis.left_motors[LEFT_LIFT].set_brake_mode(pros::E_MOTOR_BRAKE_HOLD);
    chassis.right_motors[RIGHT_LIFT].set_brake_mode(pros::E_MOTOR_BRAKE_HOLD);

    chassis.initialize();
    ez::as::initialize();
}

/**
 * Runs while the robot is in the disabled state of Field Management System or
 * the VEX Competition Switch, following either autonomous or opcontrol. When
 * the robot is enabled, this task will exit.
 */
void disabled() {
    flywheel.stop();
    // . . .
}

/**
 * Runs after initialize(), and before autonomous when connected to the Field
 * Management System or the VEX Competition Switch. This is intended for
 * competition-specific initialization routines, such as an autonomous selector
 * on the LCD.
 *
 * This task will exit when the robot is enabled and autonomous or opcontrol
 * starts.
 */
void competition_initialize() {
    // . . .
}

/**
 * Runs the user autonomous code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the autonomous
 * mode. Alternatively, this function may be called in initialize or opcontrol
 * for non-competition testing purposes.
 *
 * If the robot is disabled or communications is lost, the autonomous task
 * will be stopped. Re-enabling the robot will restart the task, not re-start it
 * from where it left off.
 */
void autonomous() {
    chassis.reset_pid_targets(); // Resets PID targets to 0
    chassis.reset_gyro(); // Reset gyro position to 0
    chassis.reset_drive_sensor(); // Reset drive sensors to 0
    chassis.set_drive_brake(MOTOR_BRAKE_HOLD); // Set motors to hold.  This helps autonomous consistency.

    // skillsAuton();
    ez::as::auton_selector.call_selected_auton(); // Calls selected auton from autonomous selector.
}

// Function to control the lift
void liftOpControl() {
  // If the UP button is pressed
  if (master.get_digital(DIGITAL_X)) {
    // Set the lift speed to maximum upward speed
    setLiftSpeed(12000);
  }
  // Else if the DOWN button is pressed
  else if (master.get_digital(DIGITAL_B)) {
    // Set the lift speed to maximum downward speed
    setLiftSpeed(-12000);
  }
  // If no buttons are pressed
  else {
    // Stop the lift
    setLiftSpeed(0);
  }

  // If the RIGHT button is newly pressed
  if (master.get_digital_new_press(DIGITAL_UP)) {
    // Toggle the PTO to true (activate the PTO)
    togglePTO(false);
  }
}

/**
 * Runs the operator control code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the operator
 * control mode.
 *
 * If no competition control is connected, this function will run immediately
 * following initialize().
 *
 * If the robot is disabled or communications is lost, the
 * operator control task will be stopped. Re-enabling the robot will restart the
 * task, not resume it from where it left off.
 */
void opcontrol() {
    // chassis.reset_pid_targets(); // Resets PID targets to 0
    // chassis.reset_gyro(); // Reset gyro position to 0
    // chassis.reset_drive_sensor(); // Reset drive sensors to 0
    // chassis.set_drive_brake(MOTOR_BRAKE_HOLD); // Set motors to hold.  This helps autonomous consistency.

    // skillsAuton();
    

    // while (master.get_analog(ANALOG_LEFT_Y) <= 10 || master.get_analog(ANALOG_RIGHT_Y) <= 0){
    //     pros::delay(ez::util::DELAY_TIME);
    // }
    // chassis.set_mode(ez::DISABLE);
    // flywheel.stop();
    // Initialize the clock to the current time in milliseconds
    chassis.set_mode(ez::DISABLE);
    std::uint32_t clock = sylib::millis();

    // Set the brake mode of the drive motors to coast
    chassis.set_drive_brake(MOTOR_BRAKE_COAST);

    // Initialize the flywheel state and target speed
    bool isFlywheelRunning = false;
    double flywheelTargetSpeed = 0;
    // Initialize a counter for the master print statement
    int counter = 0;

    // Main control loop
    while (true) {
        // Use tank control for the chassis
        chassis.tank();

        // Call the lift control function
        liftOpControl();

        // If the R1 button is pressed, set the intake speed to maximum
        if (master.get_digital(pros::E_CONTROLLER_DIGITAL_R1)) intake = 127;
        // If the R2 button is pressed, set the intake speed to maximum in the opposite direction
        else if (master.get_digital(pros::E_CONTROLLER_DIGITAL_R2)) intake = -127;
        // If neither R1 nor R2 is pressed, stop the intake
        else intake = 0;

          // If the L1 button is newly pressed, toggle the state of the wings piston
    if (master.get_digital_new_press(pros::E_CONTROLLER_DIGITAL_L1)) {
      wingsPistonState = !wingsPistonState;
      wingsPiston.set_value(wingsPistonState);
    }

    // if (master.get_digital_new_press(pros::E_CONTROLLER_DIGITAL_L2)) {
    //   master.print(2, 0, "%d", lift_position());
    //   liftTo(140);
    // }

    // if (master.get_digital_new_press(pros::E_CONTROLLER_DIGITAL_L2)) {
    //    setLiftPID(200);
    // }

    // If the A button is newly pressed, toggle the state of the blocker piston
    if (master.get_digital_new_press(pros::E_CONTROLLER_DIGITAL_Y)) {
      lockerPistonState = !lockerPistonState;
      lockerPiston.set_value(lockerPistonState);
    }

    // If the X button is newly pressed, toggle the state of the flywheel
    if (master.get_digital_new_press(pros::E_CONTROLLER_DIGITAL_A)) {
      if (isFlywheelRunning) {
        // If the flywheel is running, stop it
        flywheelTargetSpeed = 0;
        flywheel.stop();
      } else {
        // If the flywheel is not running, start it at a target speed of 1800
        flywheelTargetSpeed = 1800;
        flywheel.set_velocity_custom_controller(flywheelTargetSpeed);
      }
      // Toggle the flywheel state
      isFlywheelRunning = !isFlywheelRunning;
    }
        // if (master.get_digital_new_press(DIGITAL_Y)) {
        //     double targetpos = lift_position();
        //     while (master.get_digital(DIGITAL_Y)) {
        //         chassis.set_tank(-25, -25);
        //         if (fabs(lift_position() - targetpos) > 10) { setLiftSpeed(20); }
        //         pros::delay(ez::util::DELAY_TIME);
        //     }
        //     setLiftSpeed(0);
        // }

        // Every 20 iterations of the loop, print the state of the flywheel and the PTO to the master controller
        if (counter % 20)
      master.print(2, 0, "FW: %s, PTO: %s        ",
                   isFlywheelRunning ? "ON" : "OFF",
                   ptoOnLift ? "LIFT      " : "DRIVE       ");

        // Increment the counter
        counter++;
        // Delay until the next iteration of the loop
        sylib::delay_until(&clock, ez::util::DELAY_TIME);
    }
}
