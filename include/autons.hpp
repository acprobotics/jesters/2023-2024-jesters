#pragma once

#include "main.h"
inline unsigned long long constexpr operator""_ft(unsigned long long ft) {
  return ft * 12;
}
inline long double constexpr operator""_ft(long double ft) { return ft * 12; }
inline unsigned long long constexpr operator""_in(unsigned long long in) {
  return in;
}
inline long double constexpr operator""_in(long double in) { return in; }

extern Drive chassis;

void allianceSide();
void opponentSide();
void skillsAuton();
void noAuton();

extern sylib::Motor flywheel;
extern pros::Motor intake;

extern bool ptoOnLift;

extern pros::ADIDigitalOut wingsPiston;
extern pros::ADIDigitalOut lockerPiston;
extern pros::ADIDigitalOut ptoPiston;

// Declare the functions
double lift_position();

void togglePTO(bool active);
void setLiftSpeed(int input);
double liftBangBang(double current, double target);
void liftTo(double targetValue);



void default_constants();

void exit_condition_defaults();